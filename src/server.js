const express = require('express');
const cors = require('cors')
const multer = require('multer')

const AssinaXMLDiplomado = require('./AssinaXMLDiplomado')
const AssinaXMLDocumentacao = require('./AssinaXMLDocumentacao')

const app = express();
const upload = multer({dest: './.temp'})

app.use(cors());
app.use(express.json());

// CONFIGURA ROTAS DE XML DO DIPLOADO DA APLICAÇÃO
app.post('/XMLDiplomado/inicializa', upload.single('documento'), AssinaXMLDiplomado.inicializa)
app.post('/XMLDiplomado/finaliza', upload.single('documento'), AssinaXMLDiplomado.finaliza)
app.post('/XMLDiplomado/copia-nodo', AssinaXMLDiplomado.copiaNodo)

// CONFIGURA ROTAS DE XML DE DOCUMENTACAO ACADÊMICA DA APLICAÇÃO
app.post('/XMLDocumentacaoAcademica/inicializa', upload.single('documento'), AssinaXMLDocumentacao.inicializa)
app.post('/XMLDocumentacaoAcademica/finaliza', upload.single('documento'), AssinaXMLDocumentacao.finaliza)

app.listen(3333);