const FormData = require('form-data');
const fs = require('fs');
const { TextEncoder } = require('util');
const path = require('path');
const atob = require('atob');
const axios = require('axios');
const {token} = require('./credenciais');
const {server_url} = require('./configuracao')

const returnType = 'LINK';

class AssinaXMLDocumentacao {
  // PASSO 1 DA ASSINATURA: INICIALIZAÇÃO
  async inicializa(req, res) {
    console.log("Inicializando assinatura de XML Documentação");
    // RECEBE OS DADOS E O ARQUIVO DA REQUISIÇÃO QUE VEM DO FRONTEND E CRIA UM FORMDATA
    const certificate = req.body.certificate;
    const tipoAssinante = req.body.tipoAssinante;
    const documento = req.file;
        
    const form = new FormData();

    form.append('nonce', 1);
    form.append('signatureFormat', 'ENVELOPED');
    form.append('hashAlgorithm', 'SHA256');
    form.append('certificate', certificate);
    form.append('returnType', 'BASE64');
    form.append('originalDocuments[0][nonce]', 1);
    form.append('originalDocuments[0][content]', fs.createReadStream(path.resolve(__dirname, '..', '.temp', documento.filename)));


    // FAZ UM TIPO DE ASSINATURA DIFERENTE PARA CADA TIPO DE ASSINANTE
    switch (tipoAssinante) {
      case 'Representantes':
        form.append('profile', 'ADRC');
        form.append('originalDocuments[0][specificNode][namespace]', 'http://portal.mec.gov.br/diplomadigital/arquivos-em-xsd');
        form.append('originalDocuments[0][specificNode][name]', 'DadosDiploma');

        break;

      case 'IESEmissoraDadosDiploma':
        form.append('profile', 'ADRC');
        form.append('originalDocuments[0][specificNode][namespace]', 'http://portal.mec.gov.br/diplomadigital/arquivos-em-xsd');
        form.append('originalDocuments[0][specificNode][name]', 'DadosDiploma');
        form.append('includeXPathEnveloped', 'false');
        
        break;
      
      case 'IESEmissoraRegistro':
          form.append('profile', 'ADRA');
          form.append('includeXPathEnveloped', 'false');
          
          break;
    }

    // CRIA O HEADER DA REQUISIÇÃO COM O TOKEN CONFIGURADO ANTERIORMENTE
    const header = {
      headers: {
        "Authorization": token,
        "Content-Type": `multipart/form-data; boundary=${form.getBoundary()}`}};

    // FAZ A REQUISIÇÃO DE INICIALIZAÇÃO PARA A API DE ASSINATURA DA BRy
    // RESPONDE PARA O FRONT O RETORNO DA API BRy PARA QUE SEJA
    // REALIZADA A CIFRAGEM COM A EXTENSÃO DA BRY
    try {
      const response = await axios.post(server_url + '/api/xml-signature-service/v2/signatures/initialize', form, header);
      return res.json(response.data);
    } catch (err) {
      return res.status(400).send(err);
    }
  }

  // PASSO 3 DA ASSINATURA: FINALIZAÇÃO
  async finaliza(req, res) {
    console.log("Finalizando assinatura de XML Documentação");

    // RECEBE OS DADOS CIFRADOS PELA EXTENSÃO E O ARQUIVO DA REQUISIÇÃO
    // QUE VEM DO FRONTEND E CRIA UM FORMDATA
    const certificate = req.body.certificate;
    const initializedDocuments = req.body.initializedDocuments;
    const tipoAssinante = req.body.tipoAssinante;
    const cifrado = req.body.cifrado;
    const documento = req.file;

    const form = new FormData();
 
    // ADICIONA DADOS NO FORMDATA REFERENTE AO TIPO DE ASSINATURA
    form.append('nonce', 1);
    form.append('signatureFormat', 'ENVELOPED');
    form.append('hashAlgorithm', 'SHA256');
    form.append('certificate', certificate);
    form.append('profile', tipoAssinante === 'IESEmissoraRegistro' ? 'ADRA' : 'ADRC',);
    if (tipoAssinante !== 'Representantes') {
        form.append('includeXPathEnveloped', 'false');
    }
    form.append('returnType', returnType);
    form.append('finalizations[0][content]', fs.createReadStream(path.resolve(__dirname, '..', '.temp', documento.filename)));
    form.append('finalizations[0][signatureValue]', cifrado);
    form.append('finalizations[0][initializedDocument]', initializedDocuments);

    // CRIA O HEADER DA REQUISIÇÃO COM O TOKEN CONFIGURADO ANTERIORMENTE
    const header = {
      headers: {
        "Authorization": token,
        'Content-Type': `multipart/form-data; boundary=${form.getBoundary()}`,
      },
    };

    // FAZ A REQUISIÇÃO DE FINALIZAÇÃO PARA A API DE ASSINATURA DA BRy
    // RESPONDE PARA O FRONT O RETORNO DA API BRy
    try {
      const response = await axios.post(server_url + '/api/xml-signature-service/v2/signatures/finalize', form, header);

      fs.unlinkSync(path.resolve(__dirname, '..', '.temp', documento.filename))
      
      /**
       * CONSIDERANDO QUE O RETURNTYPE ESTÁ CONFIGURADO COMO 'BASE64' NA REQUISIÇÃO,
       * ESTAMOS ENVIANDO  SALVANDO EM UM REPOSITÓRIO LOCAL
       * CASO QUERIA FAZER DOWNLOAD PELO FRONTEND, BASTA ALTERAR O RETURNTYPE PARA LINK":
       */

      switch (returnType) {
        case 'LINK':
          return res.json(response.data);
        case 'BASE64':
          const codificadorDeTexto = new TextEncoder();
          const arquivoAssinado = atob(
            codificadorDeTexto.encode(response.data),
          );
          fs.writeFile(
            path.resolve(__dirname, '..', 'XMLsAssinados') + path.sep + "exemplo-diploma-documentacao-assinado.xml" ,
            arquivoAssinado,
            { encoding: 'base64' },
            err => {
              if (err) {
                return res
                  .status(400)
                  .json({ message: 'Erro ao salvar arquivo' });
              }
              console.log('Arquivo assinado e salvo localmente');
            },
          );
          return res.json({ message: 'Arquivo assinado e salvo localmente' });

        default:
          break;
      }
    } catch (err) {
      console.log(err);
      return res
        .status(400)
        .json({ error: 'Não foi possivel finalizar a assinatura' });
    }
  }
}

module.exports = new AssinaXMLDocumentacao();
