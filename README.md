# API de Geração de Assinatura de Diploma Digital com a BRy Extension

Este é exemplo backend de integração dos serviços da API de assinatura com clientes baseados em tecnologia Node.js, que utilizam a BRy Extension. 

Este exemplo apresenta os passos necessários para a geração de assinatura de diploma utilizando-se chave privada armazenada em disco.
  - Passo 1: Recebimento do conteúdo do certificado digital e do documento a ser assinado.
  - Passo 2: Inicialização da assinatura e produção dos artefatos signedAttributes.
  - Passo 3: Envio dos dados para cifragem no frontend.
  - Passo 4: Recebimento dos dados cifrados no frontend.
  - Passo 5: Finalização da assinatura e obtenção do artefato assinado.

### Tech

O exemplo utiliza das bibliotecas Node abaixo:
* [Node] - Node.js is a JavaScript runtime built on Chrome's V8 JavaScript engine.
* [Express] - Fast, unopinionated, minimalist web framework for Node.js
* [Axios] - Promise based HTTP client for the browser and node.js

### Frontend

Esta API em Node deve ser executada juntamente com o [frontend](https://gitlab.com/brytecnologia-team/integracao/api-diploma-digital/react/geracao-diploma-extensao), desenvolvido em React, para que seja feita a cifragem dos dados com a extensão para web: "BRy Extension".

### Variáveis que devem ser configuradas

O exemplo por consumir a API de assinatura necessita ser configurado com token de acesso válido.

Esse token de acesso pode ser obtido através da documentação disponibilizada no [Docs da API de Assinatura](https://api-assinatura.bry.com.br) ou através da conta de usuário no [BRy Cloud](https://cloud.bry.com.br/home/usuarios/autenticado/aplicacoes).

Caso ainda não esteja cadastrado, [cadastre-se](https://www.bry.com.br/) para ter acesso a nossa plataforma de serviços.

Além disso, no processo de geração de assinatura é obrigatório a posse de um certificado digital que identifica o autor do artefato assinado que será produzido.

Por esse motivo, é necessário configurar a BRy Extension no seu browser, assim como ter um certificado importado.

Por se tratar de uma informação sensível do usuário, reforçamos que a informação inserida no exemplo é utilizada pontualmente para a produção da assinatura.

| Variável  |                   Descrição                   | Arquivo de Configuração |  
| --------- | --------------------------------------------- | ----------------------- |
| token | Access Token para o consumo do serviço (JWT). | src/credenciais.js |
| server_url | Endereço do servidor da BRy | src/configuracao.js |
## Adquirir um certificado digital

É muito comum no início da integração não se conhecer os elementos mínimos necessários para consumo dos serviços.

Para assinar digitalmente um documento, é necessário, antes de tudo, possuir um certificado digital, que é a identidade eletrônica de uma pessoa ou empresa.

O certificado, na prática, consiste em um arquivo contendo os dados referentes à pessoa ou empresa, protegidos por criptografia altamente complexa e com prazo de validade pré-determinado.

Os elementos que protegem as informações do arquivo são duas chaves de criptografia, uma pública e a outra privada. Sendo estes elementos obrigatórios para a execução deste exemplo.

**Entendido isso, como faço para obter meu certificado digital?**

[Obtenha agora](https://certificado.bry.com.br/certificate-issue-selection) um Certificado Digital Corporativo de baixo custo para testes de integração.

Entenda mais sobre o [Certificado Corporativo](https://www.bry.com.br/blog/certificado-digital-corporativo/).  

### Uso

Para execução da aplicação de exemplo, importe o projeto em sua IDE de preferência e instale as dependências. Utilizamos o Node versão 12 e yarn (ou npm) para a instalação das dependências.

Comandos:

Instalar dependências:

    -yarn
    
ou
    
    -npm install

Executar programa:

    -yarn dev
    
ou
    
    -npm run dev

  [Node]: <https://nodejs.org/en/>
  [Express]: <https://expressjs.com/>
  [Axios]: <https://github.com/axios/axios>
  [Multer]: <https://github.com/expressjs/multer>
